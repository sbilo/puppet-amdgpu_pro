class amdgpu_pro::install inherits amdgpu_pro {

  $filename = basename($amdgpu_pro::source);

  $dirname = basename($amdgpu_pro::source,'.tar.xz');

  archive { "${amdgpu_pro::install_dir}/${filename}":
    ensure           => present,
    extract          => true,
    extract_path     => $amdgpu_pro::install_dir,
    source           => $amdgpu_pro::source,
    download_options => '-Le https://support.amd.com/en-us/kb-articles/Pages/AMDGPU-PRO-Install.aspx'
  }

  file { '/etc/apt/sources.list.d/amdgpu-pro.list':
    ensure           => present,
    content          => "deb [ trusted=yes ] file:${amdgpu_pro::install_dir}/${dirname} ./"
  }

  exec { '/usr/bin/apt-get update':
    refreshonly => true,
    subscribe   => File['/etc/apt/sources.list.d/amdgpu-pro.list']
  }

  package { [ 'amdgpu-pro-core',
              'ids-amdgpu-pro',
              'clinfo-amdgpu-pro',
              'opencl-amdgpu-pro-icd',
              'amdgpu-pro-dkms',
              'libdrm2-amdgpu-pro',
              'libdrm-amdgpu-pro-amdgpu1',
              'libopencl1-amdgpu-pro']:
    ensure          => latest,
    require         => File['/etc/apt/sources.list.d/amdgpu-pro.list'],
    notify          => Reboot['amdgpu_pro:reboot'],
  }

  reboot { 'amdgpu_pro:reboot':
    apply  => finished,
  }

  file { '/usr/bin/amdmeminfo':
    ensure => present,
    source => 'puppet:///modules/amdgpu_pro/amdmeminfo',
    owner  => 'root',
    group  => 'root',
    mode   => '0744'
  }

  file { '/usr/bin/ohgodatool':
    ensure => present,
    source => 'puppet:///modules/amdgpu_pro/ohgodatool',
    owner  => 'root',
    group  => 'root',
    mode   => '0744'
  }

  file { '/usr/bin/ohgodadecode':
    ensure => present,
    source => 'puppet:///modules/amdgpu_pro/ohgodadecode',
    owner  => 'root',
    group  => 'root',
    mode   => '0744'
  }

  file { '/usr/bin/overclock':
    content => template('amdgpu_pro/overclock.sh.erb'),
    mode    => '755',
    owner   => 'root',
    group   => 'root',
  }

  file { '/etc/overclock.d':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
  }
}