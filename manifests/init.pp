# Class: amdgpu_pro
# ===========================
#
# Full description of class amdgpu_pro here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Examples
# --------
#
# @example
#    class { 'amdgpu_pro':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <sander.bilo@gmail.com>
#
# Copyright
# ---------
#
# Copyright 2017 Sander Bilo, unless otherwise noted.
#
class amdgpu_pro(
  String $version                   = '17.20-445420',
  Stdlib::AbsolutePath $install_dir = '/opt/amdgpu-pro',
  String $source                    = "https://www2.ati.com/drivers/linux/ubuntu/amdgpu-pro-${amdgpu_pro::version}.tar.xz",

) {

  contain amdgpu_pro::install
}
