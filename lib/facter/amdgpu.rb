Facter.add(:amdgpu_amount) do
  setcode do
    Facter::Util::Resolution.exec('lspci -d 1002::0300 |wc -l')
  end
end

Facter.add(:amdgpu_rx470_count) do
  setcode do
    Facter::Util::Resolution.exec('lspci -d 1002::0300 |grep "Device 67df (rev cf)" |wc -l')
  end
end

Facter.add(:amdgpu_rx570_count) do
  setcode do
    Facter::Util::Resolution.exec('lspci -d 1002::0300 |grep "Device 67df (rev ef)" |wc -l')
  end
end

Facter.add(:amdgpu_rx580_count) do
  setcode do
    Facter::Util::Resolution.exec('lspci -d 1002::0300 |grep "Device 67df (rev e7)" |wc -l')
  end
end